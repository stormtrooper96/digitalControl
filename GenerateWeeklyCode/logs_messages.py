import datetime as dt

start = 'Start assing code weekly '
save_excel = 'create excel file '
start_taskes = 'Start to read queries'
send_files = 'Send mail with code'
finish = 'Finish process to build.'
readconfig= 'Config File exists and is readable'


def print_log(message, dic_format={}):
    dic_format.update({'time': dt.datetime.now().replace(microsecond=0)})
    message = '{time}. ' + message
    message = message.format(**dic_format)
    print (message)
