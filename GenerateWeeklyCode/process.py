# -*- coding: utf-8 -*-
import os
import sys
import constants as cts
import logs_messages as lm
from connections import execute_query,update_query,parser_connection
import smtplib
from random import *
import time

def config_project():
    if os.path.isfile(cts.CONFIG_FILE) and os.access(cts.CONFIG_FILE, os.R_OK):
        print (lm.readconfig)
    else:
        print ("Either file is missing or is not readable")
        sys.exit(1)
def executequery():
    query_path = os.path.join(cts.SQL_DIR_PATH, cts.prueba)
    query = open(query_path, 'r')
    query = query.read()
    global df
    df = execute_query(cts.CONFIG_FILE, query)
    max = len(df.index)
    print('numero de columnas a actualizar:  ',max)
    for x in range(0, max):
        idr= df.iat[x,0]
        print('Actualizando el id ',  idr )
        codigo = randint(1000000, 9999999)
        query_path = os.path.join(cts.SQL_DIR_PATH, cts.update)
        query = open(query_path, 'r')
        query = query.read().format(var=idr, codigo=codigo)
        update_query(cts.CONFIG_FILE, query)
def send_email():
    fromaddr = parser_connection(cts.CONFIG_FILE, cts.server_user)['fromaddr']
    acces_token = parser_connection(cts.CONFIG_FILE, cts.server_pass)['pass']
    date = time.strftime("%d/%m/%Y")
    TO = fromaddr
    SUBJECT = ('Codigo semana: {a}'.format(a=date))
    max = len(df.index)
    for x in range(0, max):
        codigo = df.iat[x, 2]
        email = df.iat[x, 1]
        nombre = df.iat[x, 3]
        TO = email
        TEXT = ('Hola {a}, \n Su codigo para esta semana es: {b}'.format(b=codigo, a=nombre))
        gmail_sender = fromaddr
        gmail_passwd = acces_token
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_sender, gmail_passwd)
        BODY = '\r\n'.join(['To: %s' % TO,
                            'From: %s' % gmail_sender,
                            'Subject: %s' % SUBJECT,
                            '', TEXT])
        try:
            server.sendmail(gmail_sender, [TO], BODY)
            print('email sent')
        except:
            print('error sending mail')
        server.quit()
