CREATE TABLE permisos (
  "PERCodigo" NUMBER(4),

  CONSTRAINT permisos_pk PRIMARY KEY ("PERCodigo"),
  CONSTRAINT check_PERCodigo CHECK ("PERCodigo" BETWEEN 1 AND 9999),
  "PERDescripcion" VARCHAR2(45) NOT NULL,
  CONSTRAINT chk_PERDescripcion CHECK (REGEXP_LIKE("PERDescripcion",
  '[a-z,A-Z]/s')),
  "PERConsulta" NUMBER(1) NOT NULL,
  CONSTRAINT chk_per1 CHECK (
  "PERConsulta" = 1
  OR "PERConsulta" = 0
  ),
  "PERModificar" NUMBER(1) NOT NULL,
  CONSTRAINT chk_per2 CHECK (
  "PERModificar" = 1
  OR "PERModificar" = 0
  ),
  "PERAsignar" NUMBER(1) NOT NULL,
  CONSTRAINT chk_per3 CHECK (
  "PERAsignar" = 1
  OR "PERAsignar" = 0
  ),
  "PERrecibir" NUMBER(1) NOT NULL,
  CONSTRAINT chk_per4 CHECK (
  "PERrecibir" = 1
  OR "PERrecibir" = 0
  )
);
CREATE SEQUENCE percodigo_seq
START WITH 1
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9999;

CREATE TABLE oficinas (
  "OFIid" NUMBER(4),
  CONSTRAINT oficinas_pk PRIMARY KEY ("OFIid"),
  CONSTRAINT check_OFIid CHECK ("OFIid" BETWEEN 1 AND 9999),
  "OFIdescripcion" VARCHAR2(45) NOT NULL,
  CONSTRAINT check_descof CHECK (REGEXP_LIKE("OFIdescripcion",
  '[a-z,A-Z]/s')),
  "OFIBarrio" VARCHAR2(45) NOT NULL,
  CONSTRAINT check_barrio CHECK (REGEXP_LIKE("OFIBarrio",
  '[a-z,A-Z]/s')),
  "OFIciudad" VARCHAR2(45) NOT NULL,
  CONSTRAINT check_oficiudad CHECK (REGEXP_LIKE("OFIciudad",
  '[a-z,A-Z]/s'))
);
CREATE SEQUENCE ofiid_seq
START WITH 1
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9999;


CREATE TABLE usuarios (
  "USUid" NUMBER(4),
  CONSTRAINT usuarios_pk PRIMARY KEY ("USUid"),
  CONSTRAINT check_USUid CHECK ("USUid" BETWEEN 1 AND 9999),

  "USUCodigo" VARCHAR2(45) NOT NULL,
  CONSTRAINT check_usucodigo CHECK (REGEXP_LIKE("USUCodigo",
  '[a-z]')),

  "USUClave" VARCHAR2(45) NOT NULL,
  CONSTRAINT checkUSUClave CHECK (REGEXP_LIKE("USUClave",
  '[a-z,A-Z,1-9]')),
  "USUNombre" VARCHAR2(45) NOT NULL,
  CONSTRAINT checkUSUNombre CHECK (REGEXP_LIKE("USUNombre",
  '[A-Z]/s')),
  "PERCodigo" NUMBER(4) NOT NULL,
  CONSTRAINT fk_permisos FOREIGN KEY ("PERCodigo")
  REFERENCES permisos ("PERCodigo"),
  CONSTRAINT check_PERCodigofk CHECK ("PERCodigo" BETWEEN 1 AND 9999),

  "USUCorreo" VARCHAR2(45) NOT NULL,
  CONSTRAINT check_email CHECK (REGEXP_LIKE("USUCorreo",
  '[a-zA-Z0-9_\-]+@([a-zA-Z0-9_\-]+\.)+(com|co|edu.co|.com.co|au)')),
  "USUCodigoval" NUMBER(6) NOT NULL UNIQUE,
  CONSTRAINT check_USUCodigoval CHECK ("USUCodigoval" BETWEEN 100000 AND 999999),
  "OFICodigo" NUMBER(4) NOT NULL,
  CONSTRAINT fk_oficina FOREIGN KEY ("OFICodigo")
  REFERENCES oficinas ("OFIid"),
  CONSTRAINT check_OFICodigofk CHECK ("OFICodigo" BETWEEN 1 AND 9999)
);



CREATE SEQUENCE usuid_seq
START WITH 1
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9999;
CREATE TABLE traslados (
  "NSTrasladoid" NUMBER(6),
  CONSTRAINT traslados_pk1 PRIMARY KEY ("NSTrasladoid"),
  CONSTRAINT check_NSTrasladoid CHECK ("NSTrasladoid" BETWEEN 1 AND 999999),

  "NSCreador" NUMBER(4) NOT NULL,

  CONSTRAINT fk_creador FOREIGN KEY ("NSCreador")
  REFERENCES usuarios ("USUid"),
  CONSTRAINT check_creador CHECK ("NSCreador" BETWEEN 1 AND 9999),

  "NSAsignador" NUMBER(4),
  CONSTRAINT fk_nsasignador FOREIGN KEY ("NSAsignador")
  REFERENCES usuarios ("USUid"),
  CONSTRAINT chk_asignar CHECK ("NSAsignador" <> "NSAsignado"),
  CONSTRAINT check_NSAsignador CHECK ("NSAsignador" BETWEEN 1 AND 9999),

  "NSAsignado" NUMBER(4),
  CONSTRAINT fk_nsasignado FOREIGN KEY ("NSAsignado")
  REFERENCES usuarios ("USUid"),


  CONSTRAINT check_fk_nsasignado CHECK ("NSAsignado" BETWEEN 1 AND 9999),


  "NSRecibe" NUMBER(4),
  CONSTRAINT fk_nsrecibe FOREIGN KEY ("NSRecibe")
  REFERENCES usuarios ("USUid"),
  CONSTRAINT check_fk_nsrecibe CHECK ("NSRecibe" BETWEEN 1 AND 9999),

  "NSFechaCreado" timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  "NSFechaaceptado" timestamp,
  CONSTRAINT chk_fecha1 CHECK ("NSFechaaceptado" >= "NSFechaCreado"),

  "NSFecharecibido" timestamp,
  CONSTRAINT chk_fecha2 CHECK ("NSFecharecibido" >= "NSFechaaceptado"),
  "NSfechaModifica" timestamp,
  CONSTRAINT chk_fecha3 CHECK ("NSfechaModifica" >= "NSFechaCreado"),
  "NSEstado" VARCHAR2(20) NOT NULL,
  CONSTRAINT checkNSestado CHECK (REGEXP_LIKE("NSEstado", '[A-Z]')),

  "NSorigen" NUMBER(4),
  CONSTRAINT fk_origen FOREIGN KEY ("NSorigen")
  REFERENCES oficinas ("OFIid"),
  CONSTRAINT chk_origen CHECK ("NSorigen" <> "NSDestino"),

  CONSTRAINT check_NSorigen CHECK ("NSorigen" BETWEEN 1 AND 9999),


  "NSDestino" NUMBER(4),
  CONSTRAINT check_NSDestino CHECK ("NSDestino" BETWEEN 1 AND 9999),
  CONSTRAINT fk_destino FOREIGN KEY ("NSDestino")
  REFERENCES oficinas ("OFIid"),



  "NSModifica" VARCHAR2(500),
  CONSTRAINT checkNSModifica CHECK (REGEXP_LIKE("NSModifica", '[A-Z]/s')),
  "NSCodigoval" NUMBER(6)

);
CREATE SEQUENCE traslados_seq
START WITH 1
INCREMENT BY 1
MINVALUE 1
MAXVALUE 999999;